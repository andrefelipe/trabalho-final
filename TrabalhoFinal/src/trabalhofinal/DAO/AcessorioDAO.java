

package trabalhofinal.DAO;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import trabalhofinal.Model.Acessorios;


public class AcessorioDAO {
    private Connection conex;
    
    public AcessorioDAO() throws SQLException{
        
        this.conex = CriaConexao.getConectar();
    }
    
    public void adicionar(Acessorios novoAcessorio) throws SQLException{
        int idProduto = 0;
        
        String sql = "insert into Produtos(nome, marca, fornecedor, precoVenda, quantidade, codigoDeBarras)"
               + "values(?,?,?,?,?,?)";
        
        PreparedStatement stmt = conex.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, novoAcessorio.getNome());
        stmt.setString(2, novoAcessorio.getMarca());
        stmt.setString(3, novoAcessorio.getFornecedor());
        stmt.setFloat(4, novoAcessorio.getPrecoVenda());
        stmt.setInt(5, novoAcessorio.getQuantidade());
        stmt.setString(6, novoAcessorio.getCodigoBarras());
        
        stmt.execute();
        stmt.close();
        
        PreparedStatement stmt2 = conex.prepareStatement("SELECT * FROM Produtos WHERE idProduto = LAST_INSERT_ID();");
        ResultSet rs = stmt2.executeQuery();
        while(rs.next()){
            idProduto = rs.getInt("idProduto");
        }
        
        this.adicionaAcessorio(novoAcessorio, idProduto);
        

        
    }
    
    public void adicionaAcessorio(Acessorios novoAcessorio, int idProduto) throws SQLException{
                
        String sql2 = "insert into Acessorios(idProduto, plataforma, tipoAcessorio, cor)"
                + "values(?,?,?,?)";
        
        PreparedStatement stmt2 = conex.prepareStatement(sql2);
        stmt2.setInt(1, idProduto);
        stmt2.setString(2, novoAcessorio.getPlataforma());
        stmt2.setString(3, novoAcessorio.getTipoDeAcessorio());
        stmt2.setString(4, novoAcessorio.getCor());
        
        stmt2.execute();
        stmt2.close();
        
    }
    

    
    public List<Acessorios> getLista() throws SQLException{
        
        String sql = "select p.nome, p.marca, a.tipoAcessorio, p.codigoDeBarras, p.precoVenda, p.quantidade, a.idProduto, p.idProduto, a.idAcessorio from Produtos p, Acessorios a WHERE a.idProduto = p.idProduto";
        
        PreparedStatement stmt = this.conex.prepareStatement(sql);
        
        ResultSet rs = stmt.executeQuery();
        
        List<Acessorios> acessorio = new ArrayList<Acessorios>();
        
        while(rs.next()){
            Acessorios novoAcessorio = new Acessorios();
            novoAcessorio.setNome(rs.getString("nome"));
            novoAcessorio.setMarca(rs.getString("marca"));
            novoAcessorio.setTipoDeAcessorio(rs.getString("tipoAcessorio"));
            novoAcessorio.setCodigoBarras(rs.getString("codigoDeBarras"));
            novoAcessorio.setPrecoVenda(rs.getFloat("precoVenda"));
            novoAcessorio.setQuantidade(rs.getInt("quantidade"));
            novoAcessorio.setIdAcessorios(rs.getInt("idAcessorio"));
            
            acessorio.add(novoAcessorio);
        }
        rs.close();
        stmt.close();
        return acessorio;
        
        
        }
    
    public void deletar(int idAcessorio) throws SQLException{
        int idProduto = 0;
        
        PreparedStatement stmt = conex.prepareStatement("SELECT * FROM Acessorios WHERE idAcessorio = "+idAcessorio+";");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()){
            idProduto = rs.getInt("idProduto");
        }
        
        rs.close();
        stmt.close();
        
        PreparedStatement stmt2 = conex.prepareStatement("delete from Acessorios WHERE idAcessorio = "+idAcessorio+";");        
        PreparedStatement stmt3 = conex.prepareStatement("delete from Produtos WHERE idProduto = "+idProduto+";");

        stmt2.execute();
        stmt2.close();
        stmt3.execute();
        stmt3.close();        
         
    }
    
    
}
