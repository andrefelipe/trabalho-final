

package trabalhofinal.DAO;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import trabalhofinal.Model.Cliente;



public class ClienteDAO {
    private Connection conex;
    
    public ClienteDAO() throws SQLException{
        
        this.conex = CriaConexao.getConectar();
    }
    
    public void adicionar(Cliente novoCliente) throws SQLException{
        
        String sql = "insert into Cliente(nome, cpf, idade, sexo, telefoneContato, endereco)"
               + "values(?,?,?,?,?,?)";
        
        PreparedStatement stmt = conex.prepareStatement(sql);
        stmt.setString(1, novoCliente.getNome());
        stmt.setString(2, novoCliente.getCpf());
        stmt.setString(3, novoCliente.getIdade());
        stmt.setString(4, novoCliente.getSexo());
        stmt.setString(5, novoCliente.getTelefoneContato());
        stmt.setString(6, novoCliente.getEndereco());
        
        stmt.execute();
        stmt.close();
    }
    
    public List<Cliente> getLista() throws SQLException{
        
        String sql = "select nome, cpf, telefoneContato, idCliente from Cliente";
        
        PreparedStatement stmt3 = this.conex.prepareStatement(sql);
        
        ResultSet rs = stmt3.executeQuery();
        
        List<Cliente> cliente = new ArrayList<Cliente>();
        
        while(rs.next()){
            Cliente novoCliente = new Cliente();
            novoCliente.setNome(rs.getString("nome"));
            novoCliente.setCpf(rs.getString("cpf"));
            novoCliente.setTelefoneContato(rs.getString("telefoneContato"));
            novoCliente.setIdCliente(rs.getInt("idCliente"));
            
            
            cliente.add(novoCliente);
        }
        rs.close();
        stmt3.close();
        return cliente;
        
        
        }
    public void deletar(int idCliente) throws SQLException{
        
        
        PreparedStatement stmt2 = conex.prepareStatement("delete from Cliente WHERE idCliente = "+idCliente+";");        
       
        stmt2.execute();
        stmt2.close();
        
    }
}
