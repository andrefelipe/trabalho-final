
package trabalhofinal.DAO;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import trabalhofinal.Model.Consoles;

public class ConsoleDAO {
    private Connection conex;
    
    public ConsoleDAO() throws SQLException{
        
        this.conex = CriaConexao.getConectar();
    }
    
    public void adicionar(Consoles novoConsole) throws SQLException{
        int idProduto= 0;
        
        String sql = "insert into Produtos(nome, marca, fornecedor, precoVenda, quantidade, codigoDeBarras)"
               + "values(?,?,?,?,?,?)";
        
        PreparedStatement stmt = conex.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, novoConsole.getNome());
        stmt.setString(2, novoConsole.getMarca());
        stmt.setString(3, novoConsole.getFornecedor());
        stmt.setFloat(4, novoConsole.getPrecoVenda());
        stmt.setInt(5, novoConsole.getQuantidade());
        stmt.setString(6, novoConsole.getCodigoBarras());
        
        stmt.execute();        
        stmt.close();
        
        PreparedStatement stmt2 = conex.prepareStatement("SELECT * FROM Produtos WHERE idProduto = LAST_INSERT_ID();");
        ResultSet rs = stmt2.executeQuery();
        while(rs.next()){
            idProduto = rs.getInt("idProduto");
        }
        this.adicionaConsole(novoConsole, idProduto);
        
        
    }
    
    private void adicionaConsole(Consoles novoConsole, int idProduto) throws SQLException{
                
        String sql2 = "insert into Consoles(idProduto, peso, memoria)"
                + "values(?,?,?)";
        
        PreparedStatement stmt2 = conex.prepareStatement(sql2);
        stmt2.setInt(1, idProduto);
        stmt2.setString(2, novoConsole.getPeso());
        stmt2.setString(3, novoConsole.getMemoria());
       
        stmt2.execute();
        stmt2.close();
    }

    
    public List<Consoles> getLista() throws SQLException{
        
        String sql = "select p.nome, p.marca, p.codigoDeBarras, p.precoVenda, p.quantidade, c.idProduto, p.idProduto, c.idConsole from Produtos p, Consoles c WHERE c.idProduto = p.idProduto";
        
        PreparedStatement stmt3 = this.conex.prepareStatement(sql);
        
        ResultSet rs = stmt3.executeQuery();
        
        List<Consoles> console = new ArrayList<Consoles>();
        
        while(rs.next()){
            Consoles novoConsole = new Consoles();
            novoConsole.setNome(rs.getString("nome"));
            novoConsole.setMarca(rs.getString("marca"));
            novoConsole.setCodigoBarras(rs.getString("codigoDeBarras"));
            novoConsole.setPrecoVenda(rs.getFloat("precoVenda"));
            novoConsole.setQuantidade(rs.getInt("quantidade"));
            novoConsole.setIdConsoles(rs.getInt("idConsole"));
            
            console.add(novoConsole);
        }
        rs.close();
        stmt3.close();
        return console;
        
        
        }
    
    public void deletar(int idConsole) throws SQLException{
        int idProduto = 0;
        
        PreparedStatement stmt = conex.prepareStatement("SELECT * FROM Consoles WHERE idConsole = "+idConsole+";");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()){
            idProduto = rs.getInt("idProduto");
        }
        
        rs.close();
        stmt.close();
        
        PreparedStatement stmt2 = conex.prepareStatement("delete from Consoles WHERE idConsole = "+idConsole+";");        
        PreparedStatement stmt3 = conex.prepareStatement("delete from Produtos WHERE idProduto = "+idProduto+";");

        stmt2.execute();
        stmt2.close();
        stmt3.execute();
        stmt3.close();        
         
    }
}
