
package trabalhofinal.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import trabalhofinal.Model.Funcionario;


public class FuncionarioDAO {
    private Connection conex;
    
    public FuncionarioDAO() throws SQLException{
        
        this.conex = CriaConexao.getConectar();
    }
    
    public void adicionar(Funcionario novoFuncionario) throws SQLException{
        
        String sql = "insert into Funcionario(nome, cpf, idade, sexo, numCarteiraTrabalho)"
               + "values(?,?,?,?,?)";
        
        PreparedStatement stmt = conex.prepareStatement(sql);
        stmt.setString(1, novoFuncionario.getNome());
        stmt.setString(2, novoFuncionario.getCpf());
        stmt.setString(3, novoFuncionario.getIdade());
        stmt.setString(4, novoFuncionario.getSexo());
        stmt.setString(5, novoFuncionario.getNumeroCarteiraTrabalho());
        
        stmt.execute();
        stmt.close();
    }
    
    public List<Funcionario> getLista() throws SQLException{
        
        String sql = "select nome, cpf, numCarteiraTrabalho, idFuncionario from Funcionario";
        
        PreparedStatement stmt3 = this.conex.prepareStatement(sql);
        
        ResultSet rs = stmt3.executeQuery();
        
        List<Funcionario> funcionario = new ArrayList<Funcionario>();
        
        while(rs.next()){
            Funcionario novoFuncionario = new Funcionario();
            novoFuncionario.setNome(rs.getString("nome"));
            novoFuncionario.setCpf(rs.getString("cpf"));
            novoFuncionario.setNumeroCarteiraTrabalho(rs.getString("numCarteiraTrabalho"));
            novoFuncionario.setIdFuncionario(rs.getInt("idFuncionario"));
            
            funcionario.add(novoFuncionario);
        }
        rs.close();
        stmt3.close();
        return funcionario;
        
        
        }
    public void deletar(int idFuncionario) throws SQLException{
        
        
        PreparedStatement stmt2 = conex.prepareStatement("delete from Funcionario WHERE idFuncionario = "+idFuncionario+";");        
       
        stmt2.execute();
        stmt2.close();
        
    }
    
}
