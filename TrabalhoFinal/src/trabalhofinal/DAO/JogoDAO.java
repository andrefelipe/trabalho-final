

package trabalhofinal.DAO;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import trabalhofinal.Model.Jogos;


public class JogoDAO {
    
    private Connection conex;
    
    public JogoDAO() throws SQLException{
        
        this.conex = CriaConexao.getConectar();
    }
    
    public void adicionar(Jogos novoJogo) throws SQLException{
        int idProduto=0;
        
        String sql = "insert into Produtos(nome, marca, fornecedor, precoVenda, quantidade, codigoDeBarras)"
               + "values(?,?,?,?,?,?)";
        
        PreparedStatement stmt = conex.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, novoJogo.getNome());
        stmt.setString(2, novoJogo.getMarca());
        stmt.setString(3, novoJogo.getFornecedor());
        stmt.setFloat(4, novoJogo.getPrecoVenda());
        stmt.setInt(5, novoJogo.getQuantidade());
        stmt.setString(6, novoJogo.getCodigoBarras());
        
        stmt.execute();
        stmt.close();
        
                
        PreparedStatement stmt2 = conex.prepareStatement("SELECT * FROM Produtos WHERE idProduto = LAST_INSERT_ID();");
        ResultSet rs = stmt2.executeQuery();
        while(rs.next()){
            idProduto = rs.getInt("idProduto");
        }
        
        this.adicionaJogo(novoJogo, idProduto);
        
         
    }
    

    
    private void adicionaJogo(Jogos novoJogo, int idProduto) throws SQLException{
                
                
        String sql2 = "insert into Jogos(idProduto, precoLocacao, quantidadeLocacao, plataforma, categoria, idioma)"
                + "values(?,?,?,?,?,?)";
        
        PreparedStatement stmt2 = conex.prepareStatement(sql2);
        stmt2.setInt(1, idProduto);
        stmt2.setFloat(2, novoJogo.getPrecoAluguel());
        stmt2.setInt(3, novoJogo.getQuantidadeLocacao());
        stmt2.setString(4, novoJogo.getPlataforma());
        stmt2.setString(5, novoJogo.getCategoria());
        stmt2.setString(6, novoJogo.getIdioma());
        
        
        stmt2.execute();
        stmt2.close();
        
    }
    
    public List<Jogos> getLista() throws SQLException{
        
        String sql = "select p.nome, p.marca, j.plataforma, p.codigoDeBarras, p.precoVenda, j.precoLocacao, p.quantidade, j.quantidadeLocacao, j.idProduto, p.idProduto, j.idJogo from Produtos p, Jogos j WHERE j.idProduto = p.idProduto";
        
        PreparedStatement stmt = this.conex.prepareStatement(sql);
        
        ResultSet rs = stmt.executeQuery();
        
        List<Jogos> jogo = new ArrayList<Jogos>();
        
        while(rs.next()){
            Jogos novoJogo = new Jogos();
            novoJogo.setNome(rs.getString("nome"));
            novoJogo.setMarca(rs.getString("marca"));
            novoJogo.setPlataforma(rs.getString("plataforma"));
            novoJogo.setCodigoBarras(rs.getString("codigoDeBarras"));
            novoJogo.setPrecoVenda(rs.getFloat("precoVenda"));
            novoJogo.setPrecoAluguel(rs.getFloat("precoLocacao"));
            novoJogo.setQuantidade(rs.getInt("quantidade"));
            novoJogo.setQuantidadeLocacao(rs.getInt("quantidadeLocacao"));
            novoJogo.setIdJogos(rs.getInt("idJogo"));
            
            jogo.add(novoJogo);
        }
        rs.close();
        stmt.close();
        return jogo;
        
        
        }
    
    public void deletar(int idJogo) throws SQLException{
        int idProduto = 0;
        
        PreparedStatement stmt = conex.prepareStatement("SELECT * FROM Jogos WHERE idJogo = "+idJogo+";");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()){
            idProduto = rs.getInt("idProduto");
        }
        
        rs.close();
        stmt.close();
        
        PreparedStatement stmt2 = conex.prepareStatement("delete from Jogos WHERE idJogo = "+idJogo+";");        
        PreparedStatement stmt3 = conex.prepareStatement("delete from Produtos WHERE idProduto = "+idProduto+";");

        stmt2.execute();
        stmt2.close();
        stmt3.execute();
        stmt3.close();        
         
    }
}
