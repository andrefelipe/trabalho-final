

package trabalhofinal.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.plaf.synth.SynthDesktopIconUI;
import trabalhofinal.Model.Cliente;
import trabalhofinal.Model.Venda;
import trabalhofinal.Model.VendaItem;

public class VendaDAO {
    private Connection conex;
    
    public VendaDAO() throws SQLException{
        
        this.conex = CriaConexao.getConectar();
    }
    
    public void adicionar(Venda novaVenda) throws SQLException{
        
        String sql = "insert into Servico(idFuncionario, idCliente, precoTotal)"
               + "values(?,?,?)";
        
        PreparedStatement stmt = conex.prepareStatement(sql);
        stmt.setInt(1, novaVenda.getIdFuncionario());
        stmt.setInt(2, novaVenda.getIdCliente());
        stmt.setFloat(3, 0);
        
        
        stmt.execute();
        stmt.close();
    }
    
    public int buscarValorProduto(int idProduto) throws SQLException{
        int valorProduto = 0;
        PreparedStatement stmt = conex.prepareStatement("Select * from Produtos where idProduto="+idProduto+";");
        
        ResultSet rs = stmt.executeQuery();
        while(rs.next()){
            valorProduto = rs.getInt("precoVenda");
        }
        
        return valorProduto;
    
    }
    
    public void adicionarVenda(VendaItem novoItem) throws SQLException{
        String sql = "insert into ItensVenda(idProduto, idServico, quantidade)"
               + "values(?,?,?)";
        
        PreparedStatement stmt = conex.prepareStatement(sql);
        stmt.setInt(1, novoItem.getIdProduto());
        stmt.setInt(2, novoItem.getIdServico());
        stmt.setFloat(3, novoItem.getQuantidade());
        
        
        stmt.execute();
        stmt.close();
    }
    
    public int buscarIdVenda() throws SQLException{
        int idServico = 0;
        
        PreparedStatement stmt2 = conex.prepareStatement("Select MAX(idServico) AS id from Servico");
        ResultSet rs = stmt2.executeQuery();
        while(rs.next()){
            idServico = rs.getInt("id");
        }
        return idServico;
     }
    
    public void inserirValorTotal(float valor, int idServico) throws SQLException{

                
        PreparedStatement stmt = conex.prepareStatement("UPDATE Servico SET precoTotal="+valor+" WHERE idServico="+idServico+";");
        stmt.execute();
        stmt.close();
    }
    
    public List<Venda> getLista() throws SQLException{
        
        String sql = "select IdFuncionario, IdCliente, IdServico, precoTotal from Servico";
        
        PreparedStatement stmt3 = this.conex.prepareStatement(sql);
        
        ResultSet rs = stmt3.executeQuery();
        
        List<Venda> venda = new ArrayList<Venda>();
        
        while(rs.next()){
            Venda novaVenda = new Venda();
            novaVenda.setIdFuncionario(rs.getInt("IdFuncionario"));
            novaVenda.setIdCliente(rs.getInt("IdCliente"));
            novaVenda.setIdServico(rs.getInt("IdServico"));
            novaVenda.setPrecoTotal(rs.getFloat("PrecoTotal"));
            
            
            venda.add(novaVenda);
        }
        rs.close();
        stmt3.close();
        return venda;
    }
    
    
}
