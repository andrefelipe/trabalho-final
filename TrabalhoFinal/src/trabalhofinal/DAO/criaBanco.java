

package trabalhofinal.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;



public class criaBanco {
    
    private Connection conexao;

    public criaBanco() throws SQLException
    {
        this.conexao = CriaConexao.getConectar();
    }

    /**
     * Metodo com os scripts usados para a criação do banco de dados
     * e de suas respectivas tabelas e que realiza a execução do mesmo.
     * Criando o banco de dados e as tabelas necessarias para
     * a execução do aplicativo
     * @throws SQLException
     */
    public void criarBd() throws SQLException
    {
        String sql1 = "CREATE DATABASE IF NOT EXISTS TrabalhoFinal;";
        String sql2 = "USE TrabalhoFinal;";
        String sql3 = "CREATE TABLE IF NOT EXISTS `Cliente`(" +
                       "  `idCliente` INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                       "  `nome`              VARCHAR(30) NOT NULL," +
                       "  `cpf`               VARCHAR(14)," +
                       "  `idade`             VARCHAR(3)," +
                       "  `sexo`              VARCHAR(10)," +
                       "  `telefoneContato`   VARCHAR(15)," +
                       "  `endereco`          VARCHAR(50)" +
                       ");";

        String sql4 = "CREATE TABLE IF NOT EXISTS `Funcionario`(" +
                       "  `idFuncionario` INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                       "  `nome`                 VARCHAR(30) NOT NULL," +
                       "  `cpf`                  VARCHAR(14)," +
                       "  `idade`                VARCHAR(3)," +
                       "  `sexo`                 VARCHAR(10)," +
                       "  `numCarteiraTrabalho`  VARCHAR(15)" +
                       ");";
        
        String sql5 = "CREATE TABLE IF NOT EXISTS `Produtos`(" +
"  `idProduto` INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
"  `nome`              VARCHAR(30) NOT NULL," +
"  `marca`             VARCHAR(30)," +
"  `fornecedor`        VARCHAR(40)," +
"  `precoVenda`        FLOAT," +
"  `quantidade`      INT," +
"  `codigoDeBarras`    VARCHAR(30)" +
");";
        
        String sql6 = "CREATE TABLE IF NOT EXISTS `Jogos`(" +
"  `idJogo` INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
"  `idProduto` INT NOT NULL," +
"  `precoLocacao`      FLOAT," +
"  `quantidadeLocacao` INT," +
"  `plataforma`        VARCHAR(20)," +
"  `categoria`         VARCHAR(20)," +
"  `idioma`            VARCHAR(20)," +
"  FOREIGN KEY (`idProduto`) REFERENCES `Produtos`(`idProduto`)" +
");";
        
        String sql7 = "CREATE TABLE IF NOT EXISTS `Acessorios`(" +
"  `idAcessorio` INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
"  `idProduto` INT NOT NULL," +
"  `plataforma`        VARCHAR(20)," +
"  `tipoAcessorio`     VARCHAR(20)," +
"  `cor`             VARCHAR(20)," +
"  FOREIGN KEY (`idProduto`) REFERENCES `Produtos`(`idProduto`)" +
");";
        
        String sql8 = "CREATE TABLE IF NOT EXISTS `Consoles`(" +
"  `idConsole` INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
"  `idProduto` INT NOT NULL," +
"  `peso`              VARCHAR(10)," +
"  `memoria`           VARCHAR(10)," +
"  FOREIGN KEY (`idProduto`) REFERENCES `Produtos`(`idProduto`)" +
");";
        
        String sql9 = "CREATE TABLE IF NOT EXISTS `Servico`(" +
"  `idServico`  INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
"  `idCliente`  INT NOT NULL," +
"  `idFuncionario` INT NOT NULL," +
"  `precoTotal`  FLOAT," +
"  FOREIGN KEY (`idCliente`) REFERENCES `Cliente`(`idCliente`)," +
"  FOREIGN KEY (`idFuncionario`) REFERENCES `Funcionario`(`idFuncionario`)" +
");";
        
        String sql10 = "CREATE TABLE IF NOT EXISTS `ItensVenda`(" +
"  `idServico` INT NOT NULL," +
"  `idProduto` INT NOT NULL," +
"  `quantidade` int," +
"  FOREIGN KEY (`idServico`) REFERENCES `Servico`(`idServico`)," +
"  FOREIGN KEY (`idProduto`) REFERENCES `Produtos`(`idProduto`)" +
");";
        
        String sql11 = "CREATE TABLE IF NOT EXISTS `ItensAluguel`(" +
"  `idServico` INT NOT NULL," +
"  `idJogo` INT NOT NULL," +
"  `quantidade` int," +
"  `pago` BOOL," +
"  FOREIGN KEY (`idServico`) REFERENCES `Servico`(`idServico`)," +
"  FOREIGN KEY (`idJogo`) REFERENCES `Jogos`(`idJogo`)" +
");";


        PreparedStatement stmt1 = conexao.prepareStatement(sql1);
        PreparedStatement stmt2 = conexao.prepareStatement(sql2);
        PreparedStatement stmt3 = conexao.prepareStatement(sql3);
        PreparedStatement stmt4 = conexao.prepareStatement(sql4);
        PreparedStatement stmt5 = conexao.prepareStatement(sql5);
        PreparedStatement stmt6 = conexao.prepareStatement(sql6);
        PreparedStatement stmt7 = conexao.prepareStatement(sql7);
        PreparedStatement stmt8 = conexao.prepareStatement(sql8);
        PreparedStatement stmt9 = conexao.prepareStatement(sql9);
        PreparedStatement stmt10 = conexao.prepareStatement(sql10);
        PreparedStatement stmt11 = conexao.prepareStatement(sql11);

        stmt1.execute();
        stmt2.execute();
        stmt3.execute();
        stmt4.execute();
        stmt5.execute();
        stmt6.execute();
        stmt7.execute();
        stmt8.execute();
        stmt9.execute();
        stmt10.execute();
        stmt11.execute();

        stmt1.close();
        stmt2.close();
        stmt3.close();
        stmt4.close();
        stmt5.close();
        stmt6.close();
        stmt7.close();
        stmt8.close();
        stmt9.close();
        stmt10.close();
        stmt11.close();
    }
}
