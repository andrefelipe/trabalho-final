package trabalhofinal.Main;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import trabalhofinal.DAO.CriaConexao;
import trabalhofinal.DAO.criarBanco;
import trabalhofinal.View.TelaInicial;


public class Main {
    public static void main(String[] args){
        
        
        CriaConexao conect = new CriaConexao();
        criarBanco bd;

        conect.setUSER(JOptionPane.showInputDialog(null,"Entre com o USER do banco de dados","USER",JOptionPane.QUESTION_MESSAGE));
        conect.setPW(JOptionPane.showInputDialog(null,"Entre com o PASSWORD do banco de dados","PASSWORD",JOptionPane.QUESTION_MESSAGE));

        try
        {
            bd = new criarBanco();
            conect.getConectarInicial();
            bd.criarBd();
            new TelaInicial().setVisible(true);
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null,"O programa será fechado!\nUSER e/ou PASSWORD são invalidos tente outra vez!","ERRO!!!",JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }
        
    }
