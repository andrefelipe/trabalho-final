package trabalhofinal.Model;

public class Acessorios extends Produtos{

	private String nome;
	private String tipoDeAcessorio;
	private String plataforma;
	private String cor;
        int idAcessorios;

	public Acessorios(String marca, String nome) {
		super(marca);
		this.nome = nome;
	}

    public Acessorios() {
        
    }

    public int getIdAcessorios() {
        return idAcessorios;
    }

    public void setIdAcessorios(int idAcessorios) {
        this.idAcessorios = idAcessorios;
    }
    
    

	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getTipoDeAcessorio(){
		return tipoDeAcessorio;
	}
	
	public void setTipoDeAcessorio(String tipoDeAcessorio){
		this.tipoDeAcessorio = tipoDeAcessorio;
	}

	public String getPlataforma(){
		return plataforma;
	}
	
	public void setPlataforma(String plataforma){
		this.plataforma = plataforma;
	}
	
	public String getCor(){
		return cor;
	}
	
	public void setCor(String cor){
		this.cor = cor;
	}
}