package trabalhofinal.Model;

public class Consoles extends Produtos{

	private String nome;
	private String peso;
	private String memoria;
        int idConsoles;
        


	public Consoles(String marca, String nome) {
		super(marca);
		this.nome = nome;
	}

        public Consoles() {
        
        }

    public int getIdConsoles() {
        return idConsoles;
    }

    public void setIdConsoles(int idConsoles) {
        this.idConsoles = idConsoles;
    }
        
        

	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getPeso(){
		return peso;
	}
	
	public void setPeso(String peso){
		this.peso = peso;
	}

	public String getMemoria(){
		return memoria;
	}
	
	public void setMemoria(String memoria){
		this.memoria = memoria;
	}
}