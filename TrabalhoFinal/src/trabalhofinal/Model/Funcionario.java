package trabalhofinal.Model;

public class Funcionario extends Pessoa{
	private String numeroCarteiraTrabalho;
	private String numeroVendedor;
        int idFuncionario;

    public Funcionario() {
    }

        
        
    public Funcionario(String numeroCarteiraTrabalho, String numeroVendedor, String nome, String cpf, String idade, String sexo) {
        super(nome, cpf, idade, sexo);
        this.numeroCarteiraTrabalho = numeroCarteiraTrabalho;
        this.numeroVendedor = numeroVendedor;
    }

    public int getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(int idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

        

        
        
	public String getNumeroCarteiraTrabalho() {
		return numeroCarteiraTrabalho;
	}

	public void setNumeroCarteiraTrabalho(String numeroCarteiraTrabalho) {
		this.numeroCarteiraTrabalho = numeroCarteiraTrabalho;
	}
	public String getNumeroVendedor () {
		return numeroVendedor;
	}

	public void setNumeroVendedor(String numeroVendedor) {
		this.numeroVendedor = numeroVendedor;
	}
}
