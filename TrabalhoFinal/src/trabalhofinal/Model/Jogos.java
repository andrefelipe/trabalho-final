package trabalhofinal.Model;

public class Jogos extends Produtos{
	
	private String nome;
	private String plataforma;
	private String categoria;
	private String idioma;
        private Float precoAluguel;
        private int quantidadeLocacao;
        int idJogos;

        public Jogos(){
        }
	
	public Jogos(String marca, String nome) {
		super(marca);
		this.nome = nome;
	}

    public int getIdJogos() {
        return idJogos;
    }

    public void setIdJogos(int idJogos) {
        this.idJogos = idJogos;
    }

        
        
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria= categoria;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
        
        public Float getPrecoAluguel() {
        return precoAluguel;
    }

    public void setPrecoAluguel(Float precoAluguel) {
        this.precoAluguel = precoAluguel;
    }
    
    public int getQuantidadeLocacao() {
        return quantidadeLocacao;
    }
    public void setQuantidadeLocacao(int quantidadeLocacao) {
        this.quantidadeLocacao = quantidadeLocacao;
    }
}