package trabalhofinal.View;
import java.util.ArrayList;

import trabalhofinal.Model.Funcionario;

	public class CadastroFuncionario {
		ArrayList<Funcionario> listaFuncionario = new ArrayList<>();

                
		
		public void adicionar(Funcionario novoFuncionario) {
			listaFuncionario.add(novoFuncionario);
		}
		public void remover(Funcionario novoFuncionario) {
			listaFuncionario.remove(novoFuncionario);
		}
		public Funcionario buscar(String umNome) {
			for (Funcionario novoFuncionario: listaFuncionario){
				if (novoFuncionario.getNome().equalsIgnoreCase(umNome)){
					return novoFuncionario;
				}
			}
			
			return null;
			
		}

	}

