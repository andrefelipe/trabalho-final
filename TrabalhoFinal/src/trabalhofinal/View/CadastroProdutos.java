package trabalhofinal.View;
import trabalhofinal.Model.Produtos;

import java.util.ArrayList;

public class CadastroProdutos {
	ArrayList<Produtos> listaProdutos;

	public CadastroProdutos(ArrayList<Produtos> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}
	
	public void adicionar(Produtos umProduto) {
		listaProdutos.add(umProduto);
	}
	public void remover(Produtos umProduto) {
		listaProdutos.remove(umProduto);
	}
	public Produtos buscar(String umNome) {
		for (Produtos umProduto: listaProdutos){
			if (umProduto.getMarca().equalsIgnoreCase(umNome)){
				return umProduto;
			}
		}
		
		return null;
		
	}

}