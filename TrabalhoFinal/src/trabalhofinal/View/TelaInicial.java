package trabalhofinal.View;

import java.awt.Rectangle;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import trabalhofinal.Model.Funcionario;

public class TelaInicial extends javax.swing.JFrame {
    
    public void centralizeFrame(){
    int x;
    int y;
    Rectangle scr = this.getGraphicsConfiguration().getBounds();
    Rectangle form = this.getBounds();
    x = (int) (scr.getWidth() - form.getWidth()) / 2;
    y = (int) (scr.getHeight() - form.getHeight()) / 2;
    this.setLocation(x,y);
    }

    ArrayList<Funcionario> listaFuncionario = new ArrayList<>();

    public TelaInicial() {
        initComponents();
        this.centralizeFrame();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BListagemJogos = new javax.swing.JButton();
        BListagemConsoles = new javax.swing.JButton();
        BListagemFuncionario = new javax.swing.JButton();
        BListagemAcessorios = new javax.swing.JButton();
        BListagemClientes = new javax.swing.JButton();
        BVenda = new javax.swing.JButton();
        BListaVendas = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        BListagemJogos.setText("Jogos");
        BListagemJogos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BListagemJogosActionPerformed(evt);
            }
        });

        BListagemConsoles.setText("Consoles");
        BListagemConsoles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BListagemConsolesActionPerformed(evt);
            }
        });

        BListagemFuncionario.setText("Funcionarios");
        BListagemFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BListagemFuncionarioActionPerformed(evt);
            }
        });

        BListagemAcessorios.setText("Acessorios");
        BListagemAcessorios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BListagemAcessoriosActionPerformed(evt);
            }
        });

        BListagemClientes.setText("Clientes");
        BListagemClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BListagemClientesActionPerformed(evt);
            }
        });

        BVenda.setText("Venda");
        BVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BVendaActionPerformed(evt);
            }
        });

        BListaVendas.setText("Registro Vendas");
        BListaVendas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BListaVendasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(BListagemConsoles, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BListagemFuncionario, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(BListagemClientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BListagemAcessorios, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(BListagemJogos, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(108, 108, 108))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(BVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BListaVendas, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BListagemFuncionario)
                    .addComponent(BListagemClientes))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BListagemConsoles)
                    .addComponent(BListagemAcessorios))
                .addGap(18, 18, 18)
                .addComponent(BListagemJogos)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BVenda)
                    .addComponent(BListaVendas))
                .addContainerGap(64, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BListagemFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BListagemFuncionarioActionPerformed
        TelaListagemFuncionario listagem;
        try {
            listagem = new TelaListagemFuncionario();
            listagem.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.dispose();
    }//GEN-LAST:event_BListagemFuncionarioActionPerformed

    private void BListagemJogosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BListagemJogosActionPerformed
        TelaListagemJogos listagemj;
        try {
            listagemj = new TelaListagemJogos();
            listagemj.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.dispose();
    }//GEN-LAST:event_BListagemJogosActionPerformed

    private void BListagemConsolesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BListagemConsolesActionPerformed
        TelaListagemConsoles listagemc;
        try {
            listagemc = new TelaListagemConsoles();
            listagemc.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.dispose();    }//GEN-LAST:event_BListagemConsolesActionPerformed

    private void BListagemAcessoriosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BListagemAcessoriosActionPerformed
        TelaListagemAcessorios listagema;
        try {
            listagema = new TelaListagemAcessorios();
            listagema.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.dispose();
    }//GEN-LAST:event_BListagemAcessoriosActionPerformed

    private void BListagemClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BListagemClientesActionPerformed
        TelaListagemClientes listagemcli;
        try {
            listagemcli = new TelaListagemClientes();
            listagemcli.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.dispose();
    }//GEN-LAST:event_BListagemClientesActionPerformed

    private void BVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BVendaActionPerformed
        TelaVenda venda;
        venda = new TelaVenda();
        venda.setVisible(true);
        
        
        this.dispose();
    }//GEN-LAST:event_BVendaActionPerformed

    private void BListaVendasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BListaVendasActionPerformed
        TelaListaVendas listaVenda;
        try {
            listaVenda = new TelaListaVendas();
            listaVenda.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.dispose();
    }//GEN-LAST:event_BListaVendasActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaInicial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BListaVendas;
    private javax.swing.JButton BListagemAcessorios;
    private javax.swing.JButton BListagemClientes;
    private javax.swing.JButton BListagemConsoles;
    private javax.swing.JButton BListagemFuncionario;
    private javax.swing.JButton BListagemJogos;
    private javax.swing.JButton BVenda;
    // End of variables declaration//GEN-END:variables
}
