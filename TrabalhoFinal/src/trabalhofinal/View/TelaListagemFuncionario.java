package trabalhofinal.View;

import java.awt.Rectangle;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import trabalhofinal.DAO.FuncionarioDAO;
import trabalhofinal.Model.Funcionario;

public class TelaListagemFuncionario extends javax.swing.JFrame {
    public void centralizeFrame(){
    int x;
    int y;
    Rectangle scr = this.getGraphicsConfiguration().getBounds();
    Rectangle form = this.getBounds();
    x = (int) (scr.getWidth() - form.getWidth()) / 2;
    y = (int) (scr.getHeight() - form.getHeight()) / 2;
    this.setLocation(x,y);
}
    public TelaListagemFuncionario() throws SQLException {
        initComponents();
        atualizaTabela();
        this.centralizeFrame();
        
    }
       
    public static void atualizaTabela() throws SQLException{
        DefaultTableModel tTabela = (DefaultTableModel)TFuncionario.getModel();
        tTabela.setNumRows(0);
        FuncionarioDAO funcionario = new FuncionarioDAO();
        
        List<Funcionario> Listafuncionario = funcionario.getLista();
        
        for (int linha = 0; linha < Listafuncionario.size(); linha++){
            
            Funcionario funcionarios = Listafuncionario.get(linha);
            
            tTabela.addRow(new Object[]{1});
            
            
            TFuncionario.setValueAt(funcionarios.getIdFuncionario(), linha, 0);
            TFuncionario.setValueAt(funcionarios.getNome(), linha, 1);
            TFuncionario.setValueAt(funcionarios.getCpf(), linha, 2);
            TFuncionario.setValueAt(funcionarios.getNumeroCarteiraTrabalho(), linha, 3);
            
            
            
        }
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TFuncionario = new javax.swing.JTable();
        BVoltar = new javax.swing.JButton();
        BRemover = new javax.swing.JButton();
        BCadastro = new javax.swing.JButton();
        BAtualizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TFuncionario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id", "Nome", "CPF", "Carteira de Trabalho"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(TFuncionario);

        BVoltar.setText("Voltar");
        BVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BVoltarActionPerformed(evt);
            }
        });

        BRemover.setText("Remover");
        BRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BRemoverActionPerformed(evt);
            }
        });

        BCadastro.setText("Cadastrar");
        BCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BCadastroActionPerformed(evt);
            }
        });

        BAtualizar.setText("Atualizar");
        BAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BAtualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(BAtualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BCadastro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BRemover)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BVoltar))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 492, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 148, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BVoltar)
                    .addComponent(BRemover)
                    .addComponent(BCadastro)
                    .addComponent(BAtualizar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BVoltarActionPerformed
        TelaInicial inicio = new TelaInicial();
        
        inicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BVoltarActionPerformed

    private void BRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BRemoverActionPerformed
        int linhaSelecionada = TFuncionario.getSelectedRow();
        int idSelecionada = (int) TFuncionario.getValueAt(linhaSelecionada, 0);
 
    if (linhaSelecionada >= 0){
        int resposta = JOptionPane.showConfirmDialog(this, "Deseja excluir o funcionario?");
        if (resposta == JOptionPane.YES_OPTION){
            FuncionarioDAO funcionario;
            try {
                funcionario = new FuncionarioDAO();
                funcionario.deletar(idSelecionada);
            } catch (SQLException ex) {
                Logger.getLogger(TelaListagemFuncionario.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
                atualizaTabela();
            } catch (SQLException ex) {
                Logger.getLogger(TelaListagemFuncionario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    else{
        JOptionPane.showMessageDialog(this, "É necessário selecionar um funcionario", "Funcionario", JOptionPane.INFORMATION_MESSAGE);
    }
    }//GEN-LAST:event_BRemoverActionPerformed

    private void BCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BCadastroActionPerformed
        TelaCadastroFuncionario cadastro = new TelaCadastroFuncionario();
        
        cadastro.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BCadastroActionPerformed

    private void BAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BAtualizarActionPerformed
        TelaListagemFuncionario atualiza;
        try {
            atualiza = new TelaListagemFuncionario();
            atualiza.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaListagemFuncionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.dispose();
    }//GEN-LAST:event_BAtualizarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaListagemFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaListagemFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaListagemFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaListagemFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TelaListagemFuncionario().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(TelaListagemFuncionario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BAtualizar;
    private javax.swing.JButton BCadastro;
    private javax.swing.JButton BRemover;
    private javax.swing.JButton BVoltar;
    private static javax.swing.JTable TFuncionario;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
