package trabalhofinal.View;

import java.awt.Rectangle;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import trabalhofinal.DAO.JogoDAO;
import trabalhofinal.Model.Jogos;
import static trabalhofinal.View.TelaListagemFuncionario.atualizaTabela;

public class TelaListagemJogos extends javax.swing.JFrame {
    public void centralizeFrame(){
    int x;
    int y;
    Rectangle scr = this.getGraphicsConfiguration().getBounds();
    Rectangle form = this.getBounds();
    x = (int) (scr.getWidth() - form.getWidth()) / 2;
    y = (int) (scr.getHeight() - form.getHeight()) / 2;
    this.setLocation(x,y);
}
    public TelaListagemJogos() throws SQLException {
        initComponents();
        atualizaTabela();
        this.centralizeFrame();
    }

    public static void atualizaTabela() throws SQLException{
        DefaultTableModel tTabela = (DefaultTableModel)TJogos.getModel();
        tTabela.setNumRows(0);
        JogoDAO jogo = new JogoDAO();
        
        List<Jogos> Listajogo = jogo.getLista();
        
        for (int linha = 0; linha < Listajogo.size(); linha++){
            
            Jogos jogos = Listajogo.get(linha);
            
            tTabela.addRow(new Object[]{1});
            
            TJogos.setValueAt(jogos.getIdJogos(), linha, 0);
            TJogos.setValueAt(jogos.getNome(), linha, 1);
            TJogos.setValueAt(jogos.getMarca(), linha, 2);
            TJogos.setValueAt(jogos.getPlataforma(), linha, 3);
            TJogos.setValueAt(jogos.getCodigoBarras(), linha, 4);
            TJogos.setValueAt(jogos.getPrecoVenda(), linha, 5);
            TJogos.setValueAt(jogos.getPrecoAluguel(), linha, 6);
            TJogos.setValueAt(jogos.getQuantidade(), linha, 7);
            TJogos.setValueAt(jogos.getQuantidadeLocacao(), linha, 8);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TJogos = new javax.swing.JTable();
        BVoltar = new javax.swing.JButton();
        BRemover = new javax.swing.JButton();
        BCadastro = new javax.swing.JButton();
        BAtualizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TJogos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Nome", "Marca", "Plataf.", "Cod. de barras", "Preço Venda", "Preço Aluguel", "Quant. Venda", "Quant. Aluguel"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(TJogos);

        BVoltar.setText("Voltar");
        BVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BVoltarActionPerformed(evt);
            }
        });

        BRemover.setText("Remover");
        BRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BRemoverActionPerformed(evt);
            }
        });

        BCadastro.setText("Cadastrar");
        BCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BCadastroActionPerformed(evt);
            }
        });

        BAtualizar.setText("Atualizar");
        BAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BAtualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BAtualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BCadastro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BRemover)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BVoltar)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 899, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BVoltar)
                    .addComponent(BRemover)
                    .addComponent(BCadastro)
                    .addComponent(BAtualizar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BVoltarActionPerformed
        TelaInicial inicio = new TelaInicial();
        
        inicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BVoltarActionPerformed

    private void BRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BRemoverActionPerformed
        int linhaSelecionada = TJogos.getSelectedRow();
        int idSelecionado = (int) TJogos.getValueAt(linhaSelecionada, 0);
 
    if (linhaSelecionada >= 0){
        int resposta = JOptionPane.showConfirmDialog(this, "Deseja excluir o jogo?");
        if (resposta == JOptionPane.YES_OPTION){
            JogoDAO jogo;
            try {
                jogo = new JogoDAO();
                jogo.deletar(idSelecionado);
            } catch (SQLException ex) {
                Logger.getLogger(TelaListagemJogos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
                atualizaTabela();
            } catch (SQLException ex) {
                Logger.getLogger(TelaListagemJogos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    else{
        JOptionPane.showMessageDialog(this, "É necessário selecionar um jogo", "Jogo", JOptionPane.INFORMATION_MESSAGE);
    }
    }//GEN-LAST:event_BRemoverActionPerformed

    private void BCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BCadastroActionPerformed
        TelaCadastroJogo cadastroj = new TelaCadastroJogo();
        
        cadastroj.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BCadastroActionPerformed

    private void BAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BAtualizarActionPerformed
        TelaListagemJogos atualiza;
        try {
            atualiza = new TelaListagemJogos();
            atualiza.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TelaListagemJogos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        this.dispose();
    }//GEN-LAST:event_BAtualizarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaListagemJogos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaListagemJogos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaListagemJogos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaListagemJogos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TelaListagemJogos().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(TelaListagemJogos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BAtualizar;
    private javax.swing.JButton BCadastro;
    private javax.swing.JButton BRemover;
    private javax.swing.JButton BVoltar;
    private static javax.swing.JTable TJogos;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
